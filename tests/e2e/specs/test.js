// https://docs.cypress.io/api/introduction/api.html

describe("The Home Page", () => {
  it("loads the page", () => {
    cy.visit("/");
    cy.contains("h1", "Where in the world?");
  }),
    it("searches for `Australia`", () => {
      cy.get("input").type("Australia");
      cy.wait(1000);
      cy.contains("a", "Australia");
    }),
    it("clicks on `Australia` and goes to the country details page for `Australia`", () => {
      cy.get("span").click();
      cy.wait(500);
      cy.contains("h2", "Australia");
    });
});
