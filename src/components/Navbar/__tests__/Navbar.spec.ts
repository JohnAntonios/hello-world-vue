import { shallowMount } from "@vue/test-utils";
import Navbar from "../Navbar.vue";

describe("Navbar.vue", () => {
  test("should render", () => {
    const wrapper = shallowMount(Navbar, {
      stubs: ["router-link"],
    });
    expect(wrapper.text()).toMatch("Where in the world?");
  });
});
