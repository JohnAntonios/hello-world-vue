import { shallowMount } from "@vue/test-utils";
import SearchCountry from "../SearchCountry.vue";

describe("SearchCountry.vue", () => {
  test("should render", () => {
    const wrapper = shallowMount(SearchCountry, {
      stubs: ["router-link"],
    });
    const searchInput = wrapper.find("input");
    expect(searchInput.exists()).toBe(true);
  }),
    test("should show `Australia` and `Austria` in results, when `Aus` is searched for", async () => {
      const wrapper = shallowMount(SearchCountry, {
        stubs: ["router-link"],
      });
      const searchInput = wrapper.find("input");
      searchInput.trigger("input", { event: { target: { value: "Aus" } } });
      setTimeout(() => {
        const resultCountryNames = wrapper.findAll("span");
        expect(resultCountryNames.length).toBe(2);
      }, 1500);
    }, 2000);
});
