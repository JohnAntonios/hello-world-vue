import { shallowMount } from "@vue/test-utils";
import CountryDetails from "../CountryDetails.vue";

const SAMPLE_COUNTRY = {
  name: "Australia",
  flag: "https://restcountries.eu/data/aus.svg",
  population: 24117360,
  borders: [],
};

describe("CountryDetails.vue", () => {
  test("should render", () => {
    const wrapper = shallowMount(CountryDetails, {
      stubs: ["router-link"],
      propsData: {
        country: SAMPLE_COUNTRY,
      },
    });
    const countryName = wrapper.find(".country--name");
    expect(countryName.text()).toMatch(SAMPLE_COUNTRY.name);
  }),
    test("should render and show population number", () => {
      const wrapper = shallowMount(CountryDetails, {
        stubs: ["router-link"],
        propsData: {
          country: SAMPLE_COUNTRY,
        },
      });
      const countryPopulation = wrapper.find(".country--population");
      expect(countryPopulation.text()).toMatch(
        `Population: ${SAMPLE_COUNTRY.population}`
      );
    }),
    test("should render and not show border countries if an empty `borders` array is passed", () => {
      const wrapper = shallowMount(CountryDetails, {
        stubs: ["router-link"],
        propsData: {
          country: SAMPLE_COUNTRY,
        },
      });
      const borderCountries = wrapper.find(".country--border-countries");
      expect(borderCountries.exists()).toBe(false);
    }),
    test("should render and show border countries if a `borders` array is passed", () => {
      const wrapper = shallowMount(CountryDetails, {
        stubs: ["router-link"],
        propsData: {
          country: { ...SAMPLE_COUNTRY, borders: ["ENG"] },
        },
      });
      const borderCountries = wrapper.find(".country--border-countries");
      expect(borderCountries.exists()).toBe(true);
    });
});
