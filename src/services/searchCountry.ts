import ax, { Response } from "./bootstrap";

export interface SearchCountry {
  name: string;
  alpha3Code: string;
  flag: string;
}

export default async (query: string): Promise<Response<SearchCountry[]>> => {
  try {
    const res = await ax.get<SearchCountry[]>(`/name/${query}`, {
      params: {
        fields: "name;alpha3Code;flag",
      },
    });
    return {
      fail: false,
      success: true,
      data: res.data,
    };
  } catch (error) {
    console.error(error);
    return {
      fail: true,
      success: false,
    };
  }
};
