import axios from "axios";

export interface Response<T> {
  success: boolean;
  fail: boolean;
  data?: T;
}

export default axios.create({ baseURL: "https://restcountries.eu/rest/v2" });
