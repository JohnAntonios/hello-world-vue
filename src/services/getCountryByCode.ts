import ax, { Response } from "./bootstrap";

export interface CountryByCode {
  name: string;
  flag: string;
  population: number;
  borders: string[];
}

export default async (code: string): Promise<Response<CountryByCode>> => {
  try {
    const res = await ax.get<CountryByCode>(`/alpha/${code}`);
    return {
      fail: false,
      success: true,
      data: res.data,
    };
  } catch (error) {
    console.error(error);
    return {
      fail: true,
      success: false,
    };
  }
};
